---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: about
title: Arya Prasetya
invisible: true
seo:
  title: Arya Prasetya, Aspiring Physicist
---

A theoretical physicist in training.

I'm currrently a graduate student at [Leipzig University](https://www.uni-leipzig.de/). My interest is in condensed matter theory and computational physics. At the moment, I have just enrolled to the Mathematical Physics Programme in Leipzig.
